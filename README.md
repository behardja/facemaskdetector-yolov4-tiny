## Facemask Detection - Yolov4-tiny
##### Based on [Darknet Framework](https://github.com/AlexeyAB/darknet#how-to-train-tiny-yolo-to-detect-your-custom-objects)

<img src="inf/ny_video_results.gif">


[Video source](https://www.youtube.com/watch?v=JDd9K45_3H8&ab_channel=AdCouncil) by Ad Council
<br><br>


------

<br> 
Face mask detector built using a custom YOLOv4-tiny implementation on Darknet framework and TensorRT. Model supports detection for live streaming, videos files, and images from the Nvidia Jetson. Initial images sourced from publicly available datasets including Kaggle and Google image searches. Labels were generated using VIA as well as parsed and converted to YOLO-trainable text files.  
<br><br>
This project served as an exercise in transfer learning, object detection, the Nvidia CUDA and Jetson ecosystem, and edge device deployment. [Project Infographic summary](https://drive.google.com/file/d/1EoNqFo-ziS-BvswejicBJ6SXWUcBTjBd/view).
<br><br>

<img src="inf/jetson.JPG"  width="30%" height="30%">

### Trained Custom Model Performance
| Recall | Precision | IoU | FPS            |
|--------|-----------|-----|----------------|
| 93%    | 84%       | 75% | ~15 at 680x480 |


### Prerequisites

[OpenCV](http://opencv.org/) | [CUDA](https://developer.nvidia.com/cuda-downloads)
or
[NVIDIA L4T](https://developer.nvidia.com/embedded/linux-tegra)
```
$ sudo apt-get update

$ sudo apt-get install libhdf5-serial-dev hdf5-tools libhdf5-dev zlib1g-dev zip libjpeg8-dev liblapack-dev libblas-dev gfortran
```
```
# pip
$ sudo pip3 install -U pip testresources setuptools

$ sudo pip3 install -U numpy==1.16.1 future==0.18.2 mock==3.0.5 h5py==2.10.0 keras_preprocessing==1.1.1 keras_applications==1.0.8 gast==0.2.2 futures protobuf pybind11
```
### Getting Started (Linux)
Darknet Installation
```
$ git clone https://github.com/AlexeyAB/darknet.git
```
Configurations:
```
# Makefile configuration
GPU=1  
CUDNN=1  
OPENCV=1
NVCC= /usr/local/cuda/bin

# Test configuration changes
[net]
batch=1
subdivisions=1
max_batches = 4000
steps=3200,3600

[convolutional]
size=1
stride=1
filters=21

[yolo]
classes=2
random=0
```
Compile Network
```
# paths
$ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/cuda/lib64  
$ export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}

# compile
$ sudo make

# verify compile to function
$ ./darknet
```

### Running Model 
Perform Sample Inferences using Nvidia Jetson:
```
# run images using sample yolov4-tiny-custom model 
$ ./darknet detector demo data-AB/obj.data yolov4-tiny-custom-AB.cfg backup/yolov4-tiny-custom_last.weights inf/sample.jpg
```
<img src="inf/out_1.jpg"  width="47%" height="47%"> <img src="inf/out_2.jpg"  width="47%" height="47%">
```
# run real-time webcam streams using sample yolov4-tiny-custom model 
$ ./darknet detector demo data-AB/obj.data yolov4-tiny-custom-AB.cfg backup/yolov4-tiny-custom_last.weights -c 0
```
### TensorRT Deployment (coming soon)

### Additional Reference
*  Darknet and YOLO (https://pjreddie.com/darknet/)
*  Bochkovskiy, A., Wang C., Yuan M. L., (2020) "YOLOv4: Optimal Speed and Accuracy of Object Detection" (https://arxiv.org/abs/2004.10934)
* VGG Image Annotator (VIA) (http://www.robots.ox.ac.uk/~vgg/software/via/)
* Datasets
	* https://github.com/X-zhangyang/Real-World-Masked-Face-Dataset
	* Evan Danilovich (2020 March). Medical Masks Dataset. Version 1. Retrieved May 14, 2020  [https://www.kaggle.com/ivandanilovich/medical-masks-dataset](https://www.kaggle.com/ivandanilovich/medical-masks-dataset)


<!-- ![Farmers Market Finder Demo](demo/demo.gif) -->




