# find and replace

from xml.etree import ElementTree as et
import os

for datafile in os.listdir(os.getcwd()):
    tree = et.parse(datafile)
    root = tree.getroot()
   
    for boxes in root.iter('object'):
        filename = root.find('filename').text
        if boxes.find("name").text == 'mask_weared_incorrect':
            print(datafile)
            boxes.find("name").text = 'with_mask'
        tree.write(datafile)
